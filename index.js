#!/usr/bin/env node

import chalk from 'chalk';
import inquirer from 'inquirer';
import ora from 'ora';
import fs from 'fs';
import { spawn } from 'child_process';
import { createReadStream } from 'fs';
import { uploadFile } from "./lib/drive.js";

let typeOfProgram;
let fileBasePath;

const sleep = (ms = 2000) => new Promise((r) => setTimeout(r, ms));

async function Program() {
    const spinner = ora('Loading...');
    const programTitle = chalk.green('Welcome to my CLI, a program that helps to create some executables.\n')

    console.log(programTitle);

    await getTypeOfProgram();
    await getFileBasePath();

    spinner.start();
    checkPaths();
    await sleep();
    spinner.stop();
    spinner.start();
    removeFiles();
    console.log(chalk.green('\nFiles removed successfully'));
    spinner.stop();

    // Move files from target to the executable dir
    moveFiles();
    console.log(chalk.green('\nFiles moved successfully'));
    await sleep();
    createExeFile();
    console.log(chalk.green('\nProcess Completed'));

}

async function getTypeOfProgram() {
    const typeOfProgramAnswer = await inquirer.prompt([
        {
            type: 'list',
            name: 'typeOfProgram',
            message: 'What type of program do you want to create?',
            choices: ['Client', 'Server'],
        },
    ]);

    typeOfProgram = typeOfProgramAnswer.typeOfProgram;
}

async function getFileBasePath() {
    const fileBasePathAnswer = await inquirer.prompt([
        {
            type: 'input',
            name: 'fileBasePath',
            message: 'What is the base path of the file?',
        },
    ]);

    fileBasePath = fileBasePathAnswer.fileBasePath;
}

function checkPaths() {
    const dirName = typeOfProgram === 'Client' ? 'SistemaNegociosPenaDesktop' : 'SistemaNegociosPenaServer';

    if (!fs.existsSync(fileBasePath)) {
        console.log(chalk.red('Path does not exist'));
        process.exit(1);
    }

    if (!fs.existsSync(`${fileBasePath}/${dirName}/target`)) {
        console.log(chalk.red(`Path (${fileBasePath}/${dirName}/target) does not exist. Please execute the maven command to generate target directory.`));
        process.exit(1);
    }

    if (!fs.existsSync(`${fileBasePath}/${dirName}/target/SistemaNegociosPena.jar`)) {
        console.log(chalk.red('Jar Dir not exists, please check it'));
        process.exit(1);
    }

    // Validacion para client
    if (typeOfProgram === 'Client') {
        if (!fs.existsSync(`${fileBasePath}/${dirName}/target/libs`)) {
            console.log(chalk.red('Libs Dir not exists, please check it'));
            process.exit(1);
        }
    }

    // Validacion para client
    if (typeOfProgram === 'Server') {
        if (!fs.existsSync(`${fileBasePath}/${dirName}/target/SistemaNegociosPena.jar`)) {
            console.log(chalk.red('Libs Dir not exists, please check it'));
            process.exit(1);
        }
    }
}

function removeFiles() {
    const dirName = 'SistemaNegociosPenaEjecutables';
    const subDirName = typeOfProgram === 'Client' ? 'cliente' : 'server';

    if (typeOfProgram === 'Client') {
        fs.rmSync(`${fileBasePath}/${dirName}/${subDirName}/libs`, {recursive: true, force: true});
        fs.rmSync(`${fileBasePath}/${dirName}/${subDirName}/Negocios Peña Libreria Setup.exe`, {recursive: true, force: true});
        fs.rmSync(`${fileBasePath}/${dirName}/${subDirName}/Negocios Peña Libreria.exe`, {recursive: true, force: true});
        fs.rmSync(`${fileBasePath}/${dirName}/${subDirName}/SistemaNegociosPena.jar`, {recursive: true, force: true});
    }

    if (typeOfProgram === 'Server') {
        fs.rmSync(`${fileBasePath}/${dirName}/${subDirName}/SistemaNegociosPena.jar`, {recursive: true, force: true});
        fs.rmSync(`${fileBasePath}/${dirName}/${subDirName}/NegociosPenaServer.exe`, {recursive: true, force: true});
    }
}

function moveFiles() {
    const dirName = 'SistemaNegociosPenaEjecutables';
    const subDirName = typeOfProgram === 'Client' ? 'cliente' : 'server';

    if (typeOfProgram === 'Client') {
        fs.cpSync(`${fileBasePath}/SistemaNegociosPenaDesktop/target/libs`, `${fileBasePath}/${dirName}/${subDirName}/libs`, {recursive: true});
        fs.copyFileSync(`${fileBasePath}/SistemaNegociosPenaDesktop/target/SistemaNegociosPena.jar`, `${fileBasePath}/${dirName}/${subDirName}/SistemaNegociosPena.jar`);
    }

    if (typeOfProgram === 'Server') {
        fs.copyFileSync(`${fileBasePath}/SistemaNegociosPenaServer/target/SistemaNegociosPena.jar`, `${fileBasePath}/${dirName}/${subDirName}/SistemaNegociosPena.jar`);
    }
}

function createExeFile(fullPath) {
    const subDirName = typeOfProgram === 'Client' ? 'cliente' : 'server';
    const fileName = typeOfProgram === 'Client' ? 'SistemaLibreria-Cliente.jsmooth' : 'NegociosPenaServer.jsmooth';
    const jSmoothProcess = spawn('C:\\Program Files (x86)\\JSmooth 0.9.9-7\\jsmoothcmd.exe', [`${fileBasePath}/SistemaNegociosPenaEjecutables/${subDirName}/${fileName}`]);

    jSmoothProcess.stderr.on("data", (data) => {
        console.log(`stdout: ${data}`);
    });

    jSmoothProcess.on('exit', code => {
        console.log(chalk.yellow(`Process ended with ${code}`));
        if(typeOfProgram === 'Client') innoProcess();
    })
}


function innoProcess() {
    const spinner = ora('Generating setup file...').start();
    const innoProcess = spawn('C:\\Program Files (x86)\\Inno Setup 6\\ISCC.exe', [`${fileBasePath}/SistemaNegociosPenaEjecutables/cliente/Negocios Pena Libreria.iss`]);

    innoProcess.stderr.on("data", (data) => {
        console.log(`stdout: ${data}`);
    });

    innoProcess.on('exit', async (code) => {
        spinner.stop();
        console.log(chalk.yellow(`\nProcess ended with ${code}`));


        // const fileStream = createReadStream(`${fileBasePath}/SistemaNegociosPenaEjecutables/cliente/Negocios Peña Libreria Setup.exe`);
        // await uploadFile(fileStream);
    })
}

export function handleUploadFile(){

}


await Program();
