import {google} from "googleapis";
import path from 'path';

const KEY_FILE_PATH = path.join("cli-np-0f6cfcaaeb36.json");

const SCOPES = ['https://www.googleapis.com/auth/drive.file'];

const auth = new google.auth.GoogleAuth({
    keyFile: KEY_FILE_PATH,
    scopes: SCOPES,
})

/**
 * Create a new file on google drive.
 * @param {ReadStream} fileStream  The file to upload.
 */
export async function uploadFile(fileStream) {
    const drive = google.drive({ version: 'v3', auth });

    const file = await drive.files.create({
        media: {
            body: fileStream
        },
        fields: 'id',
        requestBody: {
            name: 'NegociosPeña/Cliente/NegociosPenaDesktop',
        },
    });
    console.log(file.data.id)
}

